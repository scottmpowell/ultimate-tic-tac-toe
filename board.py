import sys
import random
import numpy as np


"""
board.py
Authors: Scott Powell, Alex Merrill

We have neither given nor received unauthorized aid on this assignment
Scott Powell, Alex Merrill
"""
class GameBoard():

    def __init__(self, game_mode, depth):
        self.board = ["-"] * 81
        self.big_board = ["-"] * 9 # Keeps track of who has won a board
        self.icons = ["X", "O"] 
        self.turn = 0 # 0 is X, 1 is O

        self.last_move = None
        self.valid_min = 0 # Initially anywhere, these are updated every turn
        self.valid_max = 80
        self.game_mode = game_mode # game mode can be either "co-op" or "AI"

        self.win_conditions = [
                [0, 1, 2],
                [3, 4, 5],
                [6, 7, 8],
                [0, 3, 6],
                [1, 4, 7],
                [2, 5, 8],
                [0, 4, 8],
                [2, 4, 6]] # List of indices that result in a win

        self.depth = depth

    def begin(self):
        print("Welcome")

        
        # Begin gameplay loop
        while True:
            # based on game_mode, request_player_move/request_comp_move() infinitely loops until a valid move is selected
            ret = 0
            if self.game_mode == "co-op":
                print("Select a cell to move")
                ret = self.request_player_move()
            elif self.game_mode == "comp":
                if self.turn == 0:
                    print("Select a cell to move")
                    ret = self.request_player_move()
                else:
                    ret = self.request_comp_move(1)
            elif self.game_mode == "rand":
                if self.turn == 1:
                    ret = self.request_comp_move(1)
                else:
                    possible_moves = GameBoard.get_allowed_moves(self.board, self.last_move)
                    i = random.randint(0, len(possible_moves) - 1)
                    ret = possible_moves[i]
                    print(f"Random places 'X' on tile {ret}")
            elif self.game_mode == "AI":
                ret = self.request_comp_move(1)
            elif self.game_mode == "test":
                if self.turn == 0:
                    ret = self.request_comp_move(0)
                else:
                    ret = self.request_comp_move(1)

            ## place mark
            self.place_mark(ret, self.icons[self.turn])

            ## print board
            self.print_board()

            board_num = ret // 9 # Index of board relative to "big board"

            ## check if small board and big board is won
            if GameBoard.board_won(self.board[board_num * 9:board_num * 9 + 9]): # Slice is small board, + 10 because indexing
                print(f"{self.icons[self.turn]} has won small board {board_num}")
                
                # make all tiles in won small board show current player
                for i in range(board_num*9, board_num*9 + 9):
                    self.place_mark(i, self.icons[self.turn])
                
                # set big_board
                self.big_board[board_num] = self.icons[self.turn]
                if self.board_won(self.big_board):
                    print(f"{self.icons[self.turn]} WINS!")
                    self.print_board()
                    return self.icons[self.turn], self.board

            # set last move
            self.last_move = ret

            # we need to do ret % 9 here
            # If any player has won this small board, next move is valid anywhere
            if self.big_board[ret % 9] != '-':
                self.valid_min = 0
                self.valid_max = 80

            # Otherwise, find new minimum and max
            else:
                lowBound = (ret % 9) * 9
                upperBound = lowBound + 8
                nextMoveBoard = lowBound // 9 # Index of next board relative to "big board"
                if self.big_board[nextMoveBoard] != "-":
                    self.valid_min = 0
                    self.valid_max = 80
                else:
                    self.valid_min = lowBound
                    self.valid_max = upperBound

            self.turn = (self.turn + 1) % 2

            if len(GameBoard.all_empties(self.board)) < 1:
                print("Game is a tie.")
                return "TIE", self.board


    def check_board(self, board):
        """
        Given a 1,9 array, compares array to winning board states and returns True if the board has been won
        """
        for i in self.win_conditions:
            if board[i[0]] == board[i[1]] and board[i[1]] == board[i[2]] and board[i[0]] != '-' and board[i[0]] != "TIE":
                return True
        return False


    def request_player_move(self):
        """
        Prints list of moves and board, then requests a valid number move and returns it
        """
        self.print_help()
        self.print_board()
        while True:
            
            print(f"Player {self.icons[self.turn]}: Select a cell to move between {self.valid_min} and {self.valid_max} that is not already filled in")
            move = input()
            if move.isnumeric():
                move = int(move)

                if move >= self.valid_min and move <= self.valid_max and self.board[move] == "-" and self.big_board[move//9] == "-":
                    return move

            print(f"Move: {move} is not valid")


    def request_comp_move(self, which_eval):
        """
        returns move for AI opponent
        """
        print("Computer is choosing a move...")
        move = self.best_move(which_eval)
        ## if no move is returned, play random move from remaining moves
        if not move or move < 0 or move > 80:
            moves = GameBoard.get_allowed_moves(self.board, self.last_move)
            move = moves[random.randint(0, len(moves)-1)]
        print(f"Computer places {self.icons[self.turn]} on tile {move}")
        return move



    def best_move(self, which_eval):
        """
        returns the best move for the current player given the current board state
        """

        best = GameBoard.minimax(self.board, self.depth, self.turn, 1-self.turn, self.turn, which_eval, request_move=True, last_move=self.last_move)
        return best


    @staticmethod
    def minimax(board, depth, player, opponent, curr_player, which_eval, alpha=-np.inf, beta=np.inf, request_move=False, last_move=None):
        """
        recurisve minimax algorithm
        uses a depth limit initially set as depth
        uses alpha/beta pruning
        uses GameBoard.evaluate() eval function for estimating utility values
        """

        ## base case, return given board state eval score
        if GameBoard.board_finished(board) or depth <= 0:
            if which_eval == 0:
                return GameBoard.evaluate_0(board, player)
            else:
                return GameBoard.evaluate_1(board, player)

        ## get possible moves to recurisvely call minimax on all children of current state
        ## uses max_evaluate or min_evaluate depending on whether we are on min or max node
        possible_moves = GameBoard.get_allowed_moves(board, last_move)
        max_evaluate = -np.inf
        min_evaluate = np.inf
        best_move = None
        for move in possible_moves:
            ## make copy of board to do next moves on
            board_copy = []
            for i in board:
                board_copy.append(i)

            ## place next move
            board_copy[move] = "X" if curr_player == 0 else "O"

            ## recursive minimax step
            ## decrement depth, flip currPlayer, and send in this move as next iteration's last_move
            evaluate_minimax = GameBoard.minimax(board_copy, depth-1, player, opponent, 1 - curr_player, which_eval, alpha, beta, False, move)

            if player == curr_player: ## max nodes (moves made by the requesting player)
                ## if minimax evaluation is greater than our previous maximum evaluation, update max_evaluate and best_move
                if evaluate_minimax > max_evaluate:
                    max_evaluate = evaluate_minimax
                    best_move = move
                
                ## update alpha value for pruning
                alpha = max(evaluate_minimax, alpha)

                ## if alpha >= beta on a max node, prune (aka no need to continue to explore possible moves)
                if alpha >= beta:
                    break  
            else: ## min nodes (moves made by the opponent of the requesting player)
                ## if minimax evaluation is less than our previous minimum evaluation, update min_evaluate and best_move
                if evaluate_minimax < min_evaluate:
                    min_evaluate = evaluate_minimax
                    best_move = move
                
                ## update beta value for pruning
                beta = min(evaluate_minimax, beta)

                ## if beta <= alpha on a min node, prune (aka no need to continue to explore possible moves)
                if beta <= alpha:
                    break

        ## We have now iterated through all necessary possible moves
        ## now, depending on whether we are at a min or max node and whether a move was requested or not
        ## we can return our max/min_evaluate or the best_move if a move was requested on this call
        if player == curr_player: ## max node
            ## if move was requested on this call, return move, else return max_evaluate for recursive call
            if request_move:
                return best_move
            else:
                return max_evaluate
        else: ## min node
            ## move will never be requested for the opponents move, so just return min_evaluation
            return min_evaluate


    @staticmethod
    def evaluate_0(board, player, test=False):
        """
        this is a minimax eval function we use to calculate an estimate for the utility value of a given board state
        both this function and evaluate_1() use the same features of a board, however the features are given
        different weights evaluate_1() has a better weighting than evaluate_0(), as can be seen through running 
        the 'test' cmd line arg.

        """

        ## define player/opponent symbols
        player_sym = "X" if player == 0 else "O"
        opp_sym = "O" if player == 0 else "X"

        ## create big board
        big_board = ["-"]*9
        for i in range(len(big_board)):
            winner = GameBoard.board_won(board[i*9:9 + i*9])
            tie = GameBoard.check_board_tie(board[i*9:9 + i*9])
            if winner:
                big_board[i] = winner
            elif tie:
                big_board[i] = "TIE"


        score = 0

        ## all one distance wins for player on big board adds points
        curr_ones, opp_ones = GameBoard.min_win_distance_count(big_board, player)
        score += curr_ones*200

        ## all one distance wins for player on each small board adds points
        for i in range(8):
            if big_board[i] != "-":
                continue
            curr_ones, opp_ones = GameBoard.min_win_distance_count(board[i*9: 9 + i*9], player)
            score += curr_ones*5

        ## each board won for player adds points and each lost for player subtracts points
        for i in range(8):
            if big_board[i] == player_sym:
                score += 100
            elif big_board[i] == opp_sym:
                score -= 100

        ## if move blocks opponent one distance win on small board, add points
        ## if move causes a block for a player win on small board, minus points (this would be making a move in a tile whose possible win paths are already blocked by opponent)
        for i in range(8):
            if big_board[i] != "-":
                continue
            curr_blocked = GameBoard.num_wins_blocked(board[i*9: 9 + i*9], player)
            opp_blocked = GameBoard.num_wins_blocked(board[i*9: 9 + i*9], 1-player)
            score += curr_blocked*20
            score -= opp_blocked*20
        
        ## if move blocks opponent one distance win on big board, add points
        ## if move blocks player one distance win on big board, subtract (this would be making a move which wins a small board, but all possible paths of winning using that small board are already blocked by opponent)
        curr_blocked = GameBoard.num_wins_blocked(big_board, player)
        opp_blocked = GameBoard.num_wins_blocked(big_board, 1-player)
        score += curr_blocked*150
        score -= opp_blocked*150
        
        ## big_board is won by player adds inf, big_board lost by player subtracts inf
        ## if no moves left and no one won, game ties, make score = 0
        game_won_by = GameBoard.board_won(big_board)
        if game_won_by:
            if player_sym == game_won_by:
                score += np.inf
            else:
                score += -np.inf
        elif len(GameBoard.all_empties(board)) < 1:
            score = 0

        return score
    
    @staticmethod
    def evaluate_1(board, player, test=False):
        """
        this is the minimax eval function we use to calculate an estimate for the utility value of a given board state

        """

        ## define player/opponent symbols
        player_sym = "X" if player == 0 else "O"
        opp_sym = "O" if player == 0 else "X"

        ## create big board
        big_board = ["-"]*9
        for i in range(len(big_board)):
            winner = GameBoard.board_won(board[i*9:9 + i*9])
            tie = GameBoard.check_board_tie(board[i*9:9 + i*9])
            if winner:
                big_board[i] = winner
            elif tie:
                big_board[i] = "TIE"

        score = 0

        ## all one distance wins for player on big board adds points
        curr_ones, opp_ones = GameBoard.min_win_distance_count(big_board, player)
        score += curr_ones*200

        ## all one distance wins for player on each small board adds points
        for i in range(8):
            if big_board[i] != "-":
                continue
            curr_ones, opp_ones = GameBoard.min_win_distance_count(board[i*9: 9 + i*9], player)
            score += curr_ones*5

        ## each board won for player adds points and each lost for player subtracts points
        for i in range(8):
            if big_board[i] == player_sym:
                score += 100
            elif big_board[i] == opp_sym:
                score -= 100

        ## if move blocks opponent one distance win on small board, add points
        ## if move causes a block for a player win on small board, minus points (this would be making a move in a tile whose possible win paths are already blocked by opponent)
        for i in range(8):
            if big_board[i] != "-":
                continue
            curr_blocked = GameBoard.num_wins_blocked(board[i*9: 9 + i*9], player)
            opp_blocked = GameBoard.num_wins_blocked(board[i*9: 9 + i*9], 1-player)
            score += curr_blocked*10
            score -= opp_blocked*10
        
        ## if move blocks opponent one distance win on big board, add points
        ## if move blocks player one distance win on big board, subtract (this would be making a move which wins a small board, but all possible paths of winning using that small board are already blocked by opponent)
        curr_blocked = GameBoard.num_wins_blocked(big_board, player)
        opp_blocked = GameBoard.num_wins_blocked(big_board, 1-player)
        score += curr_blocked*120
        score -= opp_blocked*120
        
        ## big_board is won by player adds inf, big_board lost by player subtracts inf
        ## if no moves left and no one won, game ties, make score = 0
        game_won_by = GameBoard.board_won(big_board)
        if game_won_by:
            if player_sym == game_won_by:
                score += np.inf
            else:
                score += -np.inf
        elif len(GameBoard.all_empties(board)) < 1:
            score = 0

        return score


    @staticmethod
    def num_wins_blocked(board, player):
        """
        returns number of opponent wins blocked by player
        """

        player_sym = "X" if player == 0 else "O"
        opp_sym = "O" if player == 0 else "X"

        win_conditions = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]]

        count = 0
        for wincon in win_conditions:
            if board[wincon[0]] == board[wincon[1]] == opp_sym and board[wincon[2]] == player_sym:
                count += 1
            elif board[wincon[0]] == board[wincon[2]] == opp_sym and board[wincon[1]] == player_sym:
                count += 1
            elif board[wincon[1]] == board[wincon[2]] == opp_sym and board[wincon[1]] == player_sym:
                count += 1
        
        return count

        
    @staticmethod
    def min_win_distance_count(board, player):
        """
        returns a tuple of the number of one distance wins player and opponent has in format (player, opponent)
        """

        opponent = "X" if player == 1 else "O"
        player_sym = "X" if player == 0 else "O"
        win_conditions = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]]
        
        curr_dist = [3] * 8
        opp_dist = [3] * 8
        for x, wincon in enumerate(win_conditions):
            for i in wincon:
                if board[i] == opponent:
                    opp_dist[x] -= 1
                    curr_dist[x] = 100
                elif board[i] == player_sym:
                    curr_dist[x] -= 1
                    opp_dist[x] = 100
        
        curr_ones = 0
        opp_ones = 0
        for i in range(8):
            if curr_dist[i] == 1:
                curr_ones += 1
            else:
                opp_ones += 1

        return (curr_ones, opp_ones)


    @staticmethod
    def get_allowed_moves(board, last_move):
        """
        Returns all allowed moves in given board for given player from last move
        """

        if last_move is None:
            return GameBoard.all_empties(board)

        played_board = last_move // 9
        next_board = last_move % 9

        ## Fills out big_board array
        big_board = ["-"]*9
        for i in range(len(big_board)):
            winner = GameBoard.board_won(board[i*9:9 + i*9])
            tie = GameBoard.check_board_tie(board[i*9:9 + i*9])
            if winner:
                big_board[i] = winner
            elif tie:
                big_board[i] = "TIE"
        
        ## returns valid moves
        if big_board[next_board] != "-":
            return GameBoard.all_empties(board)
        else:
            values = []
            for i in range(next_board*9, 9 + next_board*9):
                if board[i] == "-":
                    values.append(i)
            return values
    
    @staticmethod
    def all_empties(board):
        """
        This method returns all empty tiles on the given board
        """
        
        values = []
        for i in range(len(board)):
            if board[i] == "-":
                values.append(i)
            
        return values

    def place_mark(self, cell, mark):
        """
        Places a mark somewhere
        """

        self.board[cell] = mark
        #self.board[move] == self.icons[self.turn]
    
    @staticmethod
    def print_board_static(board):
        """
        Prints the current board state, with - indicating an empty spot
        """
        print("")
        print(f"{board[0]} {board[1]} {board[2]} | {board[9]} {board[10]} {board[11]} | {board[18]} {board[19]} {board[20]}")
        print(f"{board[3]} {board[4]} {board[5]} | {board[12]} {board[13]} {board[14]} | {board[21]} {board[22]} {board[23]}")
        print(f"{board[6]} {board[7]} {board[8]} | {board[15]} {board[16]} {board[17]} | {board[24]} {board[25]} {board[26]}")
        print("---------------------")
        print(f"{board[27]} {board[28]} {board[29]} | {board[36]} {board[37]} {board[38]} | {board[45]} {board[46]} {board[47]}")
        print(f"{board[30]} {board[31]} {board[32]} | {board[39]} {board[40]} {board[41]} | {board[48]} {board[49]} {board[50]}")
        print(f"{board[33]} {board[34]} {board[35]} | {board[42]} {board[43]} {board[44]} | {board[51]} {board[52]} {board[53]}")
        print("---------------------")
        print(f"{board[54]} {board[55]} {board[56]} | {board[63]} {board[64]} {board[65]} | {board[72]} {board[73]} {board[74]}")
        print(f"{board[57]} {board[58]} {board[59]} | {board[66]} {board[67]} {board[68]} | {board[75]} {board[76]} {board[77]}")
        print(f"{board[60]} {board[61]} {board[62]} | {board[69]} {board[70]} {board[71]} | {board[78]} {board[79]} {board[80]}")
        print("")

    @staticmethod
    def board_won(board):
        """
        this method returns true or false depending on if a 3x3 board is won
        """
        win_conditions = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]]

        for wincon in win_conditions:
            if (board[wincon[0]] == board[wincon[1]] == board[wincon[2]]) and board[wincon[0]] != '-' and board[wincon[0]] != "TIE":
                return board[wincon[0]]

        return False

    def greed_heuristic(self, board, min_val):
        """
        This method returns the cell of a board. This cell has the greatest potential remaining winning solutions for that board.
        """
        values = [3, 2, 3, 2, 4, 2, 3, 2, 3]
        winnable = ['-', self.icons[self.turn]]
        for wincon in self.win_conditions:
            moves_needed = 3
            winning_index = -1

            for i in wincon:
                if board[i] not in winnable:
                    for x in wincon:
                        values[x] -= 1
                    values[i] = -100
                    continue
                    
                elif board[i] == winnable[1]:
                    moves_needed -= 1
                    values[i] = -100

                else:
                    winning_index = i

            if moves_needed <= 1:
                values[winning_index] = 8


        return values.index(max(values)) + min_val
    @staticmethod
    def board_finished(board):
        """
        this method returns true or false depending on if the 9x9 board is won or tied
        """
        ## makes big board
        big_board = ["-"]*9
        for i in range(len(big_board)):
            winner = GameBoard.board_won(board[i*9:9 + i*9])
            tie = GameBoard.check_board_tie(board[i*9:9 + i*9])
            if winner:
                big_board[i] = winner
            elif tie:
                big_board[i] = "TIE"
        
        ## someone won
        if GameBoard.board_won(big_board):
            return True
        
        ## checks for empty tiles, if any exist return false
        for i in range(len(board)):
            if board[i] == "-":
                return False
        
        ## tie
        return True
    
    @staticmethod
    def check_board_tie(board):
        filled = True
        for i in range(len(board)):
            if board[i] == "-":
                filled = False
        
        return filled

    def get_board(self, board_num):
        return self.board[board_num * 9: board_num * 9 + 9]

    def print_board(self):
        """
        Prints the current board state, with - indicating an empty spot
        """
        print("")
        print(f"{self.board[0]} {self.board[1]} {self.board[2]} | {self.board[9]} {self.board[10]} {self.board[11]} | {self.board[18]} {self.board[19]} {self.board[20]}")
        print(f"{self.board[3]} {self.board[4]} {self.board[5]} | {self.board[12]} {self.board[13]} {self.board[14]} | {self.board[21]} {self.board[22]} {self.board[23]}")
        print(f"{self.board[6]} {self.board[7]} {self.board[8]} | {self.board[15]} {self.board[16]} {self.board[17]} | {self.board[24]} {self.board[25]} {self.board[26]}")
        print("---------------------")
        print(f"{self.board[27]} {self.board[28]} {self.board[29]} | {self.board[36]} {self.board[37]} {self.board[38]} | {self.board[45]} {self.board[46]} {self.board[47]}")
        print(f"{self.board[30]} {self.board[31]} {self.board[32]} | {self.board[39]} {self.board[40]} {self.board[41]} | {self.board[48]} {self.board[49]} {self.board[50]}")
        print(f"{self.board[33]} {self.board[34]} {self.board[35]} | {self.board[42]} {self.board[43]} {self.board[44]} | {self.board[51]} {self.board[52]} {self.board[53]}")
        print("---------------------")
        print(f"{self.board[54]} {self.board[55]} {self.board[56]} | {self.board[63]} {self.board[64]} {self.board[65]} | {self.board[72]} {self.board[73]} {self.board[74]}")
        print(f"{self.board[57]} {self.board[58]} {self.board[59]} | {self.board[66]} {self.board[67]} {self.board[68]} | {self.board[75]} {self.board[76]} {self.board[77]}")
        print(f"{self.board[60]} {self.board[61]} {self.board[62]} | {self.board[69]} {self.board[70]} {self.board[71]} | {self.board[78]} {self.board[79]} {self.board[80]}")
        print("")

    def print_help(self):
        """
        Prints the number corresponding to each square
        """
        print("")
        print(f"00 01 02 | 09 10 11 | 18 19 20")
        print(f"03 04 05 | 12 13 14 | 21 22 23")
        print(f"06 07 08 | 15 16 17 | 24 25 26")
        print("------------------------------")
        print(f"27 28 29 | 36 37 38 | 45 46 47")
        print(f"30 31 32 | 39 40 41 | 48 49 50")
        print(f"33 34 35 | 42 43 44 | 51 52 53")
        print("------------------------------")
        print(f"54 55 56 | 63 64 65 | 72 73 74")
        print(f"57 58 59 | 66 67 68 | 75 76 77")
        print(f"60 61 62 | 69 70 71 | 78 79 80")
        print("")



## handles test running 100 gamesat depth 4

def test(depth):
    
    x_count = 0
    o_count = 0
    t_count = 0
    boards = []
    for i in range(100):
        x = GameBoard("test", depth)
        result, board = x.begin()
        boards.append(board)
        if result == "TIE":
            t_count += 1
        elif result == "X":
            x_count += 1
        elif result == "O":
            o_count += 1
    
    for board in boards:
        GameBoard.print_board_static(board)
    print(f"X_Count: {x_count}, O_Count: {o_count}, TIE_Count: {t_count}")



if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Command Line Arguments:")
        print("2-Player: 'co-op'")
        print("You vs. Computer: 'comp' 'minimax depth limit'")
        print("Random Moves vs. Computer: 'rand' 'minimax depth limit'")
        print("Computer Battle: 'AI' 'minimax depth limit'")
        print("100 Game Simulation: 'test' 'minimax depth limit'")
    elif sys.argv[1] == 'co-op':
        x = GameBoard(sys.argv[1], 0)
        x.begin()
    elif len(sys.argv) > 2 and sys.argv[2].isnumeric() and sys.argv[1] == 'test':
        test(int(sys.argv[2]))
    elif len(sys.argv) > 2 and sys.argv[2].isnumeric():
        x = GameBoard(sys.argv[1], int(sys.argv[2]))
        x.begin()
    else:
        print("Command line args wrong! Try again.")





