\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Problem Statement}{2}{section.1.1}%
\contentsline {section}{\numberline {1.2}Related Work}{3}{section.1.2}%
\contentsline {chapter}{\numberline {2}Methods}{6}{chapter.2}%
\contentsline {section}{\numberline {2.1}Simple Heuristic: Greedy}{6}{section.2.1}%
\contentsline {section}{\numberline {2.2}New Heuristic}{7}{section.2.2}%
\contentsline {section}{\numberline {2.3}Alpha- Beta- Pruning and Depth Limit}{10}{section.2.3}%
\contentsline {chapter}{\numberline {3}Results}{12}{chapter.3}%
\contentsline {chapter}{\numberline {4}Discussion}{13}{chapter.4}%
\contentsline {chapter}{\numberline {5}Conclusion}{14}{chapter.5}%
\contentsline {chapter}{Bibliography}{15}{chapter*.3}%
